#include <iostream>
#include <vector>
#include <cmath>

void _swap(int &a, int &b)
{
    int temp = a;
    a = b;
    b = temp;
}
void maxHeapify(std::vector<int> &arr, int root, int len)
{

    int leftChild = (root * 2) + 1;
    int rightChild = (root * 2) + 2;

    int maxi;
    if(leftChild < len && arr[leftChild] > arr[root])
        maxi = leftChild;
    else
        maxi = root;

    if(rightChild < len  && arr[rightChild] > arr[maxi])
        maxi = rightChild;

    if(root != maxi)
    {
        _swap(arr[root], arr[maxi]);
        maxHeapify(arr, maxi, len);
    }

}

void buildMaxHeap(std::vector<int> &arr, int len)
{
    int lastParent = floor(len / 2);

    for(int x = lastParent; x >= 0; x--)
        maxHeapify(arr, x, len);
}

void HeapSort(std::vector<int> &arr, int len)
{
    if(len > 1)
    {
        buildMaxHeap(arr, len);
        len = len - 1;
        _swap(arr[0], arr[len]);
        HeapSort(arr, len);
    }
}


void ccontent_of(std::vector<int> &v, std::string vlabel)
{
    std::cout << vlabel << std::endl;
    for(int x: v)
        std::cout << x << ' ';
    std::cout << '\n';
}
int main()
{
    std::vector<int> v {5, 4, 3, 2, 1};
    HeapSort(v, 5);

    ccontent_of(v, "vector: ");
    return 0;
}
